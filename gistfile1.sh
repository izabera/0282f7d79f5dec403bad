shopt -s dotglob nullglob
tree () (
  cd -- "$1" || exit
  prestring=$2
  [[ $prestring = *"|" ]] && prestring+="-- " || prestring+="   "
  files=(*)
  (( ${#files[@]} )) || exit
  for file in "${files[@]::${#files[@]}-1}"; do
    printf "%s%s\n" "$prestring" "$file"
    [[ -d $file ]] && tree "$file" "${prestring%-- }   |"
  done
  printf "%s%s\n" "${prestring%?-- }\`-- " "${files[-1]}"
  [[ -d ${files[-1]} ]] && tree "${files[-1]}" "${prestring%?-- }    |"
)
exec 2>/dev/null

printf '%s\n' "${1-.}"
tree "${1-.}" "|"